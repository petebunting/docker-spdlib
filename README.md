# docker-spdlib
A Docker image packaging software for LiDAR data processing used at Aberystwyth University (AU) in the Earth Observation and Ecosystem Dynamics (EOED) research group, primarily SPDLib. More information can be found at https://www.remotesensing.info.

AU-EOED software includes:

* SPDLib (https://www.spdlib.org)
* RSGISLib (https://www.rsgislib.org)


Other software:

* GDAL
* scikit-learn
* scikit-image
* matplotlib
* numpy
* pandas
* scipy
* statsmodels
* rios
* postgresql
* pylidar
* pynninterp

This image is based on the official ubuntu release with Python 3.6 and packages from ContinuumIO Miniconda3.

## Build image

docker build -t petebunting/spdlib https://petebunting@bitbucket.org/petebunting/docker-spdlib.git

## Pulling image

docker pull petebunting/spdlib

## Running Commands

To run commands, you need to mount the data path of where the data is located and where they are
mounted within the image. In this example, /home/pete/temp is mounted as /data and therefore the
data paths for the data processing need to be referenced from that path e.g., /data/TestImg.kea.

docker run -i -t -v /home/pete/temp:/data  petebunting/spdlib spdinfo /data/Testfile.spd

For tutorials in using spdlib there are some materials available at 
https://www.remotesensing.info.

